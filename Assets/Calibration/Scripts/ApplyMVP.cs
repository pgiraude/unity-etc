﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyMVP : MonoBehaviour {

	public string Status;
	public Camera Projector;
	public Matrix4x4 ProjectionMatrix4X4;
	public Matrix4x4 ModelMatrix4X4;
	public Matrix4x4 ViewMatrix4X4;
	
	[SerializeField] Transform _target;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
