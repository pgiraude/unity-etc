﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LensDistortion : MonoBehaviour
{

    public float t1, t2;
    public float k1, k2,k3;
    public Shader s;
    Material m;

    private void OnValidate()
    {
        s = Shader.Find("Effect/LensDistortion");
    }
    private void Start()
    {
        m = new Material(s);
    }
    private void Update()
    {
        m.SetFloat("k1", k1);
        m.SetFloat("k2", k2);
        m.SetFloat("k3", t1);
        m.SetFloat("k4", t2);
        m.SetFloat("k5", k3);
    }
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, m);
    }

    public float T1
    {
        get { return t1; }
        set { t1 = value; }
    }

    public float T2
    {
        get { return t2; }
        set { t2 = value; }
    }

    public float K1
    {
        get { return k1; }
        set { k1 = value; }
    }

    public float K2
    {
        get { return k2; }
        set { k2 = value; }
    }

    public float K3
    {
        get { return k3; }
        set { k3 = value; }
    }
}
