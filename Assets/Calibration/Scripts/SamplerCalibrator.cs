﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Emgu.CV;
using Object = System.Object;

public class SamplerCalibrator : MonoBehaviour
{
    public List<Vector3> xyz = new List<Vector3>();
    public List<Vector2> uv = new List<Vector2>();
    public string Status;
    public Camera Projector;
    public string Filename;
    private string _mPath;

    private LensDistortion _lensDistortionScript;
    

    public Matrix4x4 M;
    public Vector3 Pos;
    public Vector3 Rot;

    public List<float> DistortionCoefficient; 
    public bool Load;

    [SerializeField] Transform _target;


    // Use this for initialization
    void Start()
    {
        _lensDistortionScript = Projector.GetComponent< LensDistortion >();
        
        _mPath = Application.dataPath +"/" + Filename;

        if (Load)
        {
            if (File.Exists(_mPath))
            {
                string[] calibData = File.ReadAllLines(_mPath);

                for (int i = 0; i < 16; i++)
                {
                    M[i] = float.Parse(calibData[i]);
                }

                Pos = new Vector3(float.Parse(calibData[16]), float.Parse(calibData[17]), float.Parse(calibData[18]));
                Rot = new Vector3(float.Parse(calibData[19]), float.Parse(calibData[20]), float.Parse(calibData[21]));

                
                Projector.projectionMatrix = M;
                Projector.transform.localPosition = Pos;
                Projector.transform.localRotation = Quaternion.Euler(Rot);
                
//                _lensDistortionScript.T1 = float.Parse(calibData[22]); 
//                _lensDistortionScript.T2 = float.Parse(calibData[23]); 
//                _lensDistortionScript.K1 = float.Parse(calibData[24]); 
//                _lensDistortionScript.K2 = float.Parse(calibData[25]); 
//                _lensDistortionScript.K3 = float.Parse(calibData[26]); 

                Debug.Log("Projector calibration loaded");
            }
            else
            {
                Debug.Log("Projector calibration File does not exist at this path");
            }
        }

        // check if calib.txt exist
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 uv1 = Projector.ScreenToViewportPoint(Input.mousePosition);
            Vector3 xyz1 = _target.position;

            uv.Add(uv1);
            xyz.Add(xyz1);
        }

        if (Input.GetMouseButtonDown(1))
        {
            ComputeCalibration();
        }
    }

    void ComputeCalibration()
    {
        Calibration.CameraCalibrationResult result = Calibration.ComputeCameraCalibration(
            xyz.ToArray(),
            uv.ToArray(),
            new System.Drawing.Size(Projector.pixelWidth, Projector.pixelHeight),
            new Emgu.CV.Matrix<double>(3, 3),
            out Status);

        result.extrinsics.ApplyToTransform(Projector.transform);
        //Debug.Log(result.extrinsics.Position + "" + result.extrinsics.Rotation);
        Projector.projectionMatrix =
            result.intrinsics.ProjectionMatrix(Projector.nearClipPlane, Projector.farClipPlane);
        //Debug.Log(result.intrinsics.ProjectionMatrix(Projector.nearClipPlane, Projector.farClipPlane).ToString());

//        _lensDistortionScript.T1 = result.distortion.P1;
//        _lensDistortionScript.T2 = result.distortion.P2;
//        _lensDistortionScript.K1 = result.distortion.K1;
//        _lensDistortionScript.K2 = result.distortion.K2; 
//        _lensDistortionScript.K3 = result.distortion.K3;
 
        M = Projector.projectionMatrix;
        Pos = Projector.transform.localPosition;
        Rot = Projector.transform.localRotation.eulerAngles;
        
        string[] calibData = new string[27];

        for (int i = 0; i < 16; i++)
        {
            calibData[i] = M[i].ToString();
        }

        calibData[16] = Pos.x.ToString();
        calibData[17] = Pos.y.ToString();
        calibData[18] = Pos.z.ToString();
        calibData[19] = Rot.x.ToString();
        calibData[20] = Rot.y.ToString();
        calibData[21] = Rot.z.ToString();
        calibData[22] = result.distortion.K1.ToString();
        calibData[23] = result.distortion.P2.ToString();
        calibData[24] = result.distortion.K1.ToString();
        calibData[25] = result.distortion.K2.ToString();
        calibData[26] = result.distortion.K3.ToString();
        
        File.WriteAllLines(_mPath, calibData);

        print("La calibration a bien été enregistrée");
    }

    static void DebugMatrix(Matrix<double> matrix)
    {
        string m = "|";
        for (int x = 0; x < matrix.Cols; ++x)
        {
            for (int y = 0; y < matrix.Rows; ++y)
            {
                m += matrix[x, y];
                if (y < matrix.Cols - 1)
                {
                    m += "\t";
                }
                else
                {
                    m += "|\n|";
                }
            }
        }

        Debug.Log(m);
    }
}