﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookManager : MonoBehaviour
{

	public List<GameObject> pages;
	public OptitrackStreamingClient StreamingClient;

	// Use this for initialization
	void Start () {	
		if ( this.StreamingClient == null )
		{
			this.StreamingClient = OptitrackStreamingClient.FindDefaultClient();

			// If we still couldn't find one, disable this component.
			if ( this.StreamingClient == null )
			{
				Debug.LogError( GetType().FullName + ": Streaming client not set, and no " + typeof( OptitrackStreamingClient ).FullName + " components found in scene; disabling this component.", this );
				this.enabled = false;
				return;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < pages.Count; i++)
		{
			int id = pages[i].GetComponent<OptitrackRigidBody>().RigidBodyId;
			//Debug.Log(id);
			OptitrackRigidBodyState rbState = StreamingClient.GetLatestRigidBodyState( id );
			
			if (rbState != null && rbState.DeliveryTimestamp.AgeSeconds < 0.5f )
			{
				pages[i].SetActive(true);
			}else
			{
				pages[i].SetActive(false);

			}
		}
	}
}
