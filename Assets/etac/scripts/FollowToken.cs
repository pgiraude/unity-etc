﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class FollowToken : MonoBehaviour
{
	public GameObject feuilleParent;
	public String nameFeuille;
	public Vector3 scaleIni;
	public Vector3 positionIni;
	public bool follow = false;
	int cpt;

	private Collider test;
	// Use this for initialization
	
	void Start ()
	{
		feuilleParent = gameObject.transform.parent.gameObject;
		scaleIni = gameObject.transform.localScale;
		positionIni = gameObject.transform.localPosition;
		nameFeuille = feuilleParent.name;
		cpt = 0;
	}
	
	// Update is called once per frame
	void Update () {
		cpt++;
	
	}

	void OnTriggerEnter(Collider other) {
		if (cpt < 150)
			return;


		if (other.gameObject.CompareTag("token"))
		{
			if (!follow)
			{
				follow = true;
			}
		}
		
		if (other.gameObject.name == feuilleParent.name) {
			if (follow)
			{
				follow = false; 
			}
			 
			iReleaseYou();
			Debug.Log("change master");

		}
		if (cpt > 150)
			cpt = 0;
	}

	void iReleaseYou()
	{
		
		gameObject.transform.parent = feuilleParent.transform;
		gameObject.transform.localPosition = positionIni;
		gameObject.transform.localRotation = Quaternion.Euler(0,0,180);
		gameObject.transform.localScale = scaleIni;
	}
}
