﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialPosition : MonoBehaviour {

	// Use this for initialization
	private Transform initialPos;
	private GameObject feuilleIni;

	
	void Start ()
	{
		initialPos = gameObject.transform;
		feuilleIni = gameObject.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public GameObject FeuilleIni
	{
		get { return feuilleIni; }
		set { feuilleIni = value; }
	}

	public Transform InitialPos
	{
		get { return initialPos; }
		set { initialPos = value; }
	}
}
