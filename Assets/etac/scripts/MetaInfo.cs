﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class MetaInfo {
	// This class is using for storing meta-information on the content loaded as meterial. 

	public String name;
	public String tag;
	public String type; 

}
