﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceHolderToke : MonoBehaviour
{

    public List<Vector3> placeholders;
    public int cpt;
	
    // Use this for initialization
    void Start ()
    {
        cpt = 0;
        placeholders = new List<Vector3>();
        placeholders.Add(new Vector3(0.06f,-0.04f,0));
        placeholders.Add(new Vector3(0,-0.04f,-0.06f));
        placeholders.Add(new Vector3(-0.06f,-0.04f,0));
        placeholders.Add(new Vector3(0,-0.04f,0.06f));
    }
	
    // Update is called once per frame
    void Update ()
    {
        
        cpt = gameObject.transform.childCount;
        if (cpt > 0)
        {
            //displayTiny();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
         
        if (other.gameObject.CompareTag("photo"))
        {

            if (cpt > 3)
            {
                return;
            }

            if (other.gameObject.GetComponent<FollowToken>().follow)
            {
                return;
            }
            other.gameObject.transform.parent = gameObject.transform;
            other.gameObject.transform.localPosition = placeholders[cpt];
            other.gameObject.transform.localRotation = Quaternion.Euler(0,0,0);
            other.gameObject.transform.localScale = new Vector3(0.03f,0.001f,0.03f);
        }
		
    }

    void displayTiny()
    {
        if (gameObject.transform.position.y < 0.15)
        {
            foreach (Transform child in gameObject.transform)
            {
                if (child.gameObject.activeSelf)
                {
                    continue;
                }

                child.gameObject.SetActive(true);
            }
        }

        if (gameObject.transform.position.y > 0.15){
            
            foreach (Transform child in gameObject.transform)
            {
                if (!child.gameObject.activeSelf)
                {
                    continue;
                }
                child.gameObject.SetActive(false);
            } 

        }
    }
    public List<Vector3> Placeholders
    {
        get { return placeholders; }
    }
    
    
}