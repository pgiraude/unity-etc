﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceholderFeuilleHandler : MonoBehaviour {

	public List<Vector3> placeholders;
	
	// Use this for initialization
	void Start ()
	{
		//int numberofchild = gameObject.transform.childCount;
		placeholders = new List<Vector3>();
		placeholders.Add(new Vector3(0,0,0.1f));
		placeholders.Add(new Vector3(0,0,0));
		placeholders.Add(new Vector3(0,0,-0.1f));

	}
	// Update is called once per frame
	void Update () {
		
	}

	public List<Vector3> Placeholders
	{
		get { return placeholders; }
	}
}
