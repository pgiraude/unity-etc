﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDPSend : MonoBehaviour
{
    private static int localPort;

    // prefs
    public string IP; // define in init
    public int port; // define in init

    public List<Texture2D> texture2Ds;

    // "connection" things
    IPEndPoint remoteEndPoint;
    UdpClient client;


    // start from unity3d
    public void Start()
    {
        init();
    }


    // init
    public void init()
    {
        // Endpunkt definieren, von dem die Nachrichten gesendet werden.
        print("UDPSend.init()");
        // ----------------------------

        IPAddress ipAddress = IPAddress.Parse("10.204.4.145");
        remoteEndPoint = new IPEndPoint(ipAddress, 8050);
        client = new UdpClient();
    }

    // inputFromConsole
    private void inputFromConsole()
    {
        try
        {
            string text;
            do
            {
                text = Console.ReadLine();

                // Den Text zum Remote-Client senden.
                if (text != "")
                {
                    // Daten mit der UTF8-Kodierung in das Binärformat kodieren.
                    byte[] data = Encoding.UTF8.GetBytes(text);

                    // Den Text zum Remote-Client senden.
                    client.Send(data, data.Length, remoteEndPoint);
                }
            } while (text != "");
        }
        catch (Exception err)
        {
            print(err.ToString());
        }
    }

    // sendData
    public void sendString(string message)
    {
        try
        {
            //if (message != "")
            //{

            // Daten mit der UTF8-Kodierung in das Binärformat kodieren.
            byte[] data = Encoding.UTF8.GetBytes(message);


            // Den message zum Remote-Client senden.
            client.Send(data, data.Length, remoteEndPoint);
            //}
        }
        catch (Exception err)
        {
            print(err.ToString());
        }
    }

    public void sendPicture(GameObject gameObject)
    {
        try
        {
            String name = gameObject.GetComponent<MeshRenderer>().materials[0].mainTexture.name;
            Debug.Log(name);
            byte[] data = Encoding.UTF8.GetBytes(name);

            client.Send(data, data.Length, remoteEndPoint);
        }
        catch (Exception err)
        {
            print(err.ToString());
        }
    }

    // endless test
}