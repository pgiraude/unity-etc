﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomText : MonoBehaviour {

	private bool state;
	private GameObject ref_txt;
	private GameObject ref_icon;
	// Use this for initialization
	void Start () {
		state = true;
		ref_txt = GameObject.Find ("textext");
		ref_icon = GameObject.Find ("imgicon");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider other) {
		if (other.name == "textCanvas") {
			if (state) {
				ref_txt.SetActive (false);
				ref_icon.SetActive (true);
				state = false;
			} else {
				ref_txt.SetActive (true);
				ref_icon.SetActive (false);
				state = true;
			}
		}
	}
}
