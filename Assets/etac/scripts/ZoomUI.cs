﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomUI : MonoBehaviour
{
    [SerializeField]
    private RawImage _image;

    private float _currentScale;

    private float _currentX;
    private float _currentY;

    static readonly float MAX_SCALE = 0.01f;
    static readonly float MIN_SCALE = 1.0f;
    // Use this for initialization
    void Start()
    {
        _currentScale = 1.0f;
        _currentX = 0;
        _currentY = 0;
    }

    void Update()
    {
        Zoom(Input.GetAxis("Mouse ScrollWheel"));

        if (Input.GetMouseButton(2) && _currentScale < 1.0f)
        {
            if (Input.GetAxis("Mouse X") < 0)
            {
                if (_currentX > 0.0f)
                    _currentX -= 0.01f;
            }
            if (Input.GetAxis("Mouse X") > 0)
            {
                if (_currentX < 0.5f)
                    _currentX += 0.01f;
            }

            if (Input.GetAxis("Mouse Y") < 0)
            {
                if (_currentY > 0.0f)
                    _currentY -= 0.01f;
            }
            if (Input.GetAxis("Mouse Y") > 0)
            {
                if (_currentY < 0.5f)
                    _currentY += 0.01f;
            }
        }
    }

    void Zoom(float increment)
    {
        _currentScale += increment;
        if (_currentScale <= MAX_SCALE)
        {
            _currentScale = MAX_SCALE;
        }
        else if (_currentScale >= MIN_SCALE)
        {
            _currentScale = MIN_SCALE;
        }
        _image.uvRect = new Rect(_currentX, _currentY, _currentScale, _currentScale);
    }
}
