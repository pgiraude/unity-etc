﻿using NaturalPoint;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createLink : MonoBehaviour {
	
	public GameObject prefab;
	GameObject line; 
	private bool status;

	private Dictionary<GameObject, GameObject> connectionsToOthers = new Dictionary<GameObject, GameObject>(); 

	public int ID; 
	
	private int cpt;

	// Use this for initialization
	void Start () {
		
		//ID : pick a random number between 1 and 99999; 
		ID = (int) Random.Range(1, 99999);
		status = false;
		cpt = 0;
	} 	

	void Update () {
		cpt++;
	}


	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag("token") && !other.gameObject.CompareTag("photo"))
		{
			Debug.Log("collision");
			GameObject theOtherGuy = other.gameObject;
			
			if (theOtherGuy.GetComponent<createLink>().ID < ID){

				Debug.Log("My ID :" + ID + " ID de l'autre " + theOtherGuy.GetComponent<createLink>().ID);
				if (connectionsToOthers.ContainsKey(theOtherGuy))
				{
					Destroy(connectionsToOthers[theOtherGuy]);
					connectionsToOthers.Remove(theOtherGuy);
					theOtherGuy.GetComponent<createLink>().connectionsToOthers.Remove(gameObject);
				}
				else
				{
					
					line = Instantiate(prefab);
					line.GetComponent<linerender>().start = gameObject;
					line.GetComponent<linerender>().end = theOtherGuy;
					connectionsToOthers.Add(theOtherGuy, line);
					theOtherGuy.GetComponent<createLink>().connectionsToOthers.Add(gameObject,line);
				}
			}
			
			if (cpt < 60)
				return;

//			if (status) {
//				Debug.Log ("touch");
//				status = false;
//				Destroy (line);
//				return;
//
//			} else if (status == false){
//				status = true;
//				DrawLine (this.gameObject, other.gameObject);  
//			}
						
			if (cpt > 60)
				cpt = 0;
		}
	}

	public void DrawLine(GameObject start, GameObject end)
	{			
		line = Instantiate(prefab);
		line.GetComponent<linerender>().start = start;
		line.GetComponent<linerender>().end = end;
	}

	public Dictionary<GameObject, GameObject> ConnectionsToOthers
	{
		get { return connectionsToOthers; }
		set { connectionsToOthers = value; }
	}
}
