﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class linerender : MonoBehaviour {
	public GameObject start;
	public GameObject end;
	private LineRenderer line;
	private Color color;

	// Use this for initialization
	void Start () {		
		line = GetComponent<LineRenderer>();
		line.SetPosition(0, start.transform.position);
		line.SetPosition(1, end.transform.position);
		line.startColor = Color.white;
		line.endColor = Color.blue;
	}
	
	// Update is called once per frame
	void Update () {
		line.SetPosition(0, start.transform.position);
		line.SetPosition(1, end.transform.position);
	}
}
