﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class video : MonoBehaviour
{
	[SerializeField]
	private VideoPlayer _videoPlayer;
	[SerializeField]
	private AudioSource _audioSource;

	// Use this for initialization
	void Start ()
	{
		
	}

	void OnTriggerEnter(Collider other) {
		Debug.Log (other.gameObject.name);
		if (other.gameObject.name == "play") {
			if (_videoPlayer.isPlaying && _audioSource.isPlaying) {
				_audioSource.Pause ();
				_videoPlayer.Pause ();	
			} else {
				_videoPlayer.Play ();
				_audioSource.Play ();
			}
		}
	}

	// Update is called once per frame
	void Update ()
	{
		Debug.Log (_videoPlayer.isPlaying);
		if (Input.GetButtonDown ("Jump")) {

			if (_videoPlayer.isPlaying) {
				_audioSource.Pause ();
				_videoPlayer.Pause ();	
			} else {
				_videoPlayer.Play ();
				_audioSource.Play ();
			}
		}
	}

}